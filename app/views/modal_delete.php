<div class="kp-modal" id="kp-modal-delete">
  <div class="kp-modal-header">Remover página importada</div>
  <div class="kp-modal-body">
    <h2 class="kp-title">
      Você realmente deseja excluir a página
      <span data-bind="name"></span>?
    </h2>
    <form action="<?php echo plugin_url('pages', 'delete') ?>" method="POST">
      <input type="hidden" name="wp_id" data-bind="wp_id">
      <button class="kp-btn kp-btn-outline" data-dismiss="modal">Cancelar</button>
      <button class="kp-btn kp-btn-primary" type="submit">Sim</button>
    </form>
  </div>
</div>