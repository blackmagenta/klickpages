<section id="kp-alerts">
  <div class="kp-container">
    <?php if(Alert::has('success')): ?>
      <div class="kp-alert kp-alert-success">
        <?php echo Alert::get('success') ?>
        <button class="kp-btn-simple kp-pull-right" data-dismiss="alert"><b>x</b></button>
      </div>
    <?php endif ?>

    <div class="kp-alert kp-alert-warning kp-alert-off" id="kp-alert-apikey">
      <b>API Key inválida!</b> Por favor, confira novamente a sua <b>API Key</b> no seu Klickpages.
      <button class="kp-btn-simple kp-pull-right" data-dismiss="alert"><b>x</b></button>
    </div>
  </div>
</section>