<div class="kp-select-table kp-tab-content" id="kp-table-imported">
  <div class="kp-select-description">
    <h2 class="kp-title">Páginas já exportadas para Wordpress</h2>
    <p>
      Para utilizar este plugin, você irá precisar informar a sua chave de acesso no campo de API Key e salvar, feito isso e tendo o seu código validado, você poderá escolher entre as suas páginas criadas no Klickpages e transformá-las em uma página normal do seu site ou em sua página inicial.
    </p>
  </div>
  <div class="kp-search">
    <input type="text" class="kp-form-control" placeholder="Pesquise aqui a sua página...">
  </div>
  <table class="kp-table">
    <thead>
      <tr>
        <th>Título</th>
        <th>Tipo</th>
        <th colspan="1">Página Associada</th>
        <th class="sorter-false"></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($importedPages as $id => $page): ?>
        <tr>
          <td><?php echo $page['name'] ?></td>
          <td><?php echo kp_display_type($page['type']) ?></td>
          <td><?php echo $page['kp_name'] ?></td>
          <td class="kp-options">
            <a href="<?php echo $page['type'] == 'home' ? '/' : get_permalink($id) ?>" class="kp-btn-simple" title="Visualizar página" target="_blank">
              <img src="<?php echo plugin_asset('img/kp-view.png') ?>" alt="">
            </a>
            <button
              data-toggle="modal"
              data-target="#kp-modal-delete"
              data-share='{"name": "<?php echo $page['name'] ?>","wp_id": "<?php echo $id ?>"}'
              class="kp-btn-simple"
              title="Remover página importada">
              <img src="<?php echo plugin_asset('img/kp-trash.png') ?>" alt="">
            </button>
          </td>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
</div><!-- /.kp-select-table -->