<div class="kp-select-table kp-tab-content" id="kp-table-all">
  <div class="kp-select-description">
    <h2 class="kp-title">Páginas ainda no Klickpages</h2>
    <p>
      Para utilizar este plugin, você irá precisar informar a sua chave de acesso no campo de API Key e salvar, feito isso e tendo o seu código validado, você poderá escolher entre as suas páginas criadas no Klickpages e transformá-las em uma página normal do seu site ou em sua página inicial.
    </p>
  </div>
  <div class="kp-search">
    <input type="text" class="kp-form-control" placeholder="Pesquise aqui a sua página...">
  </div>
  <table class="kp-table">
    <thead>
      <tr>
        <th>Título</th>
        <th class="sorter-false"></th>
      </tr>
    </thead>
    <tbody><!-- Rows are created by table.js --></tbody>
  </table>
</div><!-- /.kp-select-table -->