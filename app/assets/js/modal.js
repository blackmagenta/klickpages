(function($) {
  $(document).ready(function() {

    $.fn.kpModal = function(action, sharedData) {
      switch(action) {
        case 'open':
          open(this, sharedData);
          break;
        case 'close':
          close(this);
          break;
      }

      this.on('click', function(e) {
        e.stopPropagation();
      });

      function open(modal, sharedData) {
        var mask = getMask();

        if(modal.parents('.kp-modal-mask') !== mask) {
          mask.append(modal);
        }

        if(typeof sharedData !== 'undefined') {
          bindSharedData(modal, sharedData);
        }

        mask.css('display', 'block');
        modal.css('display', 'block');
      }

      function close(modal) {
        var mask = getMask();
        mask.css('display', 'none');
        modal.css('display', 'none');
      }

      function getMask() {
        var mask = $('.kp-modal-mask');

        if(mask.length === 0) {
          mask = createMask();
        }

        return mask;
      }

      function createMask() {
        var mask = $('<div class="kp-modal-mask">');

        mask.on('click', function() {
          $(this).kpModal('close');
        });

        $('.kp-wrapper').append(mask);

        return mask;
      }

      function bindSharedData(modal, sharedData) {
        var objs = modal.find('[data-bind]');
        sharedData = parseSharedData(sharedData);

        objs.each(function() {
          var obj = $(this),
              attr = obj.attr('data-bind');

          obj.is('input') ? obj.val(sharedData[attr]) : obj.html(sharedData[attr]);
        });
      }

      function parseSharedData(sharedData) {
        return JSON.parse(sharedData);
      }
    };

    $.extend({
      kpModal: function(action) {
        if(action === 'load') {
          load();
        }
      }
    });

    function load() {
      $('[data-toggle="modal"]').on('click', function() {
        var modal = $($(this).attr('data-target')),
            sharedData = $(this).attr('data-share');

        modal.kpModal('open', sharedData);
      });

      $('[data-dismiss="modal"]').on('click', function(e) {
        e.preventDefault();
        var dismiss = e.target;
        var modal = dismiss.parents('.kp-modal');
        modal.kpModal('close');
      });
    }

    $.kpModal('load');
  });
})(jQuery);
