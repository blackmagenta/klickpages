<?php

class PagesController {
  /**
   * Import a page. This action called a dynamic action from
   * each type of import
   *
   * @return void
   */
  function import() {
    $action = 'import_' . $_POST['option'];

    $this->$action();

    plugin_redirect('index', 'index');
  }

  /**
   * Delete
   *
   * @return void
   */
  function delete() {
    Page::delete($_POST['wp_id']);

    kp_success('Página deletada com sucesso!');

    plugin_redirect('index', 'index');
  }

  /**
   * Import existent page
   *
   * @return boolean
   */
  private function import_existent() {
    $wpPage = get_post($_POST['wp_page']);
    $page   = $this->get_fillable_page($wpPage->post_title, 'existent');

    $this->success_imported_message($wpPage->post_title);

    return Page::create($_POST['wp_page'], $page);
  }

  /**
   * Import homepage
   *
   * @return boolean
   */
  private function import_home() {
    $page = $this->get_fillable_page('Home', 'home');

    $this->success_imported_message('Home');

    return Page::create_home(uniqid(), $page);
  }

  /**
   * Import new page
   *
   * @return boolean
   */
  private function import_new() {
    $new_page_id  = $this->create_wp_page($_POST['name']);
    $page         = $this->get_fillable_page($_POST['name'], 'page');

    $this->success_imported_message($_POST['name']);

    return Page::create($new_page_id, $page);
  }

  /**
   * Create a Wordpress page
   *
   * @param  string $name
   * @return int
   */
  private function create_wp_page($name) {
    $newPage = array(
      'post_title'      => $name,
      'post_type'       => 'page',
      'comment_status'  => 'closed',
      'ping_status'     => 'closed',
      'post_status'     => 'publish',
      'post_author'     => 1,
      'menu_order'      => 0
    );

    return wp_insert_post($newPage);
  }

  /**
   * Get fillable page
   *
   * @param  string $name
   * @param  string $type
   * @return array
   */
  private function get_fillable_page($name, $type) {
    return array(
      'name'          => $name,
      'kp_name'       => $_POST['kp_name'],
      'slug'          => $_POST['slug'],
      'domain'        => $_POST['domain'],
      'optional_path' => $_POST['optional_path'],
      'type'          => $type
    );
  }

  /**
   * Add success imported page alert message
   *
   * @param  string $name
   */
  private function success_imported_message($name) {
    $message = 'Página <b>' . $name . '</b> importada com sucesso e disponível na aba <b>JÁ IMPORTADAS</b>.';
    kp_success($message);
  }
}