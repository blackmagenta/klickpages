<?php

class App {

  static function getPageServicePort() {
    if(PLUGIN_ENV == 'development') {
      return ':8080';
    }
  }

  static function getKlickartUrl() {
    switch (PLUGIN_ENV) {
      case 'development':
        return 'http://art.klickpages.local:3000';

      case 'staging':
        return 'https://art.kpages.com.br';

      case 'production':
        return 'https://art.klickpages.com.br';
    }
  }
}
