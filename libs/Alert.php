<?php

class Alert {
  /**
   * Verify if has alert message
   *
   * @param  string  $key
   * @return boolean
   */
  static function has($key) {
    return isset($_SESSION[PLUGIN_NAME][$key]);
  }

  /**
   * Set message
   *
   * @param string $key
   * @param string $message
   */
  static function set($key, $message) {
    $_SESSION[PLUGIN_NAME][$key] = $message;
  }

  /**
   * Get message
   *
   * @param  string $key
   * @return string
   */
  static function get($key) {
    $message = $_SESSION[PLUGIN_NAME][$key];

    self::flush($key);

    return $message;
  }

  /**
   * Flush message after get
   *
   * @param  string $key
   * @return void
   */
  static private function flush($key) {
    unset($_SESSION[PLUGIN_NAME][$key]);
  }
}