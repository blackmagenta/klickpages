<?php

class Klickart {
  static function getPageInfo($slug) {
    $result = self::doRequest($slug);
    return json_decode($result);
  }

  static private function doRequest($slug) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, App::getKlickartUrl() . '/api/pages/' . $slug . '.json');
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:' . ApiKey::get()));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $output = curl_exec($ch);
    curl_close($ch);

    return $output;
  }
}