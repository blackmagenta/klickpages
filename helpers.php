<?php
/**
 * Get asset url from plugin
 *
 * @param  string $path
 * @return void
 */
function plugin_asset($path) {
  return plugins_url( 'app/assets/' . $path, __FILE__ );
}

/**
 * Return plugin url from controller and action
 *
 * @param  string $controller
 * @param  string $action
 * @param  array  $params
 * @return string
 */
function plugin_url($controller, $action, $params = array()) {
  $queryString = '';

  foreach ($params as $key => $value) {
    $queryString .= '&' . $key . '=' . $value;
  }

  return '?page=' . PLUGIN_NAME . '&c=' . $controller . '&a=' . $action . $queryString;
}

/**
 * Redirect to plugin url
 *
 * @param  string $controller
 * @param  string $action
 * @return void
 */
function plugin_redirect($controller = 'index', $action = 'index') {
  echo '<script>window.location.href="' . plugin_url($controller, $action). '"</script>';
}

/**
 * Redirect to external
 *
 * @param  string $url
 * @return void
 */
function plugin_redirect_external($url) {
  echo '<script>window.location.href="' . $url . '"</script>';
}

/**
 * Include a plugin view
 *
 * @param  string $view
 * @param  array  $data
 * @return boolean
 */
function plugin_view($view, $data) {
  extract($data);
  include PLUGIN_APP_PATH . '/views/' . $view . '.php';
}

/**
 * Returns correct page type display
 *
 * @param  string $type
 * @return string
 */
function kp_display_type($type) {
  switch ($type) {
    case 'home':
      return 'Página Inicial';
      break;

    case 'page':
      return 'Página Normal';
      break;

    case 'existent':
      return 'Página Normal';
      break;
  }
}

/**
 * Set success message
 *
 * @param  string $message
 * @return void
 */
function kp_success($message) {
  Alert::set('success', $message);
}
