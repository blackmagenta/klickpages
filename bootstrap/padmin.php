<?php

add_action('admin_menu', 'plugin_setup_admin_menu');

/**
 * Setup plugin's admin menu
 *
 * @return void
 */
function plugin_setup_admin_menu() {
  add_menu_page(
  'Klickpages',
  'Klickpages',
  'manage_options',
  'klickpages',
  'plugin_admin_page',
  plugin_asset('img/kp-icon-sm.png'));
}

/**
 * Setup plugin's router system
 *
 * @return void
 */
function plugin_admin_page() {
  plugin_admin_route();
}

/**
 * Plugin's router
 *
 * @return void
 */
function plugin_admin_route() {
  $controller = isset($_GET['c']) ? $_GET['c'] : 'index';
  $action     = isset($_GET['a']) ? $_GET['a'] : 'index';

  $controller = ucfirst($controller) . 'Controller';
  $action     = strtolower($action);
  $filepath   = PLUGIN_APP_PATH . '//controllers/' . $controller . '.php';

  include $filepath;

  $controllerObj = new $controller;
  $controllerObj->$action();
}