<?php
/**
 * WP Recommended
 * Make sure we don't expose any info if called directly
 */
if ( !function_exists( 'add_action' ) ) {
  echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
  exit;
}