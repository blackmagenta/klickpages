<?php

add_action('get_header', 'klickpages_load_page');
add_action('get_header', 'klickpages_load_home_page');

/**
 * Load klickpages's page
 *
 * @return void
 */
function klickpages_load_page() {
  global $post;

  if(is_page()) {
    $page = Page::find($post->ID);

    if($page) {
      return get_and_print_content($post->ID, $page);
    }
  }
}

/**
 * Load klickpage's page when is homepage
 *
 * @return void
 */
function klickpages_load_home_page() {
  global $post;

  if(is_home()) {
    $homeID = Page::find_home();
    $page   = Page::find($homeID);

    if($page) {
      return get_and_print_content($homeID, $page);
    }
  }
}

/**
 * Get and print page content
 *
 * @param  int   $id
 * @param  array $page
 * @return void
 */
function get_and_print_content($id, $page) {
  $page = update_page_data($id, $page); // Old pages

  $url = 'https://' . $page['domain'] . App::getPageServicePort() . '/' . $page['optional_path'];
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HEADER, false);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
  $data = curl_exec($curl);
  curl_close($curl);

  die(print($data));
}

/**
 * Update old pages
 *
 * @param  int   $id
 * @param  array $page
 * @return array
 */
function update_page_data($id, $page) {
  if($page['domain'] == null || $page['optional_path'] == null) {
    $info = Klickart::getPageInfo($page['slug']);

    if(!$info || $info->domain == null || $info->optional_path == null) {
      die(header("HTTP/1.0 404 Not Found"));
    }

    $page['domain'] = $info->domain;
    $page['optional_path'] = $info->optional_path;

    Page::update($id, $page);
  }

  return $page;
}
